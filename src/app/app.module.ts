import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatSidenavModule} from '@angular/material/sidenav';
import { LayoutModule } from './layout/layout.module';
import { ListUsersComponent } from './users/list-users/list-users.component';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import { AddUsersComponent } from './users/add-users/add-users.component';
import { EditUsersComponent } from './users/edit-users/edit-users.component';
import { DeleteUsersComponent } from './users/delete-users/delete-users.component';
import { UsersModule } from './users/users.module';
import { HttpClientModule } from '@angular/common/http';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FrontPageComponent } from './front-page/front-page.component';
 

@NgModule({
  declarations: [
    AppComponent,
    ListUsersComponent,
    // ViewUsersComponent,
    AddUsersComponent,
    EditUsersComponent,
    DeleteUsersComponent,
    FrontPageComponent,
    
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    LayoutModule,
    UsersModule,
    HttpClientModule,
    NgxMaterialTimepickerModule,
    MatInputModule,
    MatFormFieldModule,
    FlexLayoutModule
    
  ],
  exports:[ 
    UsersModule,
    NgxMaterialTimepickerModule,
    MatInputModule,
    MatFormFieldModule,
    FlexLayoutModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
