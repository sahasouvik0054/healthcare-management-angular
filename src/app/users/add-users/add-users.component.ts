import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, FormsModule, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UserService } from 'src/app/services/user.service';
import { MediaChange, MediaObserver } from '@angular/flex-layout';
import { Subscription } from 'rxjs';
import { v4 as uuid } from 'uuid';

@Component({
  selector: 'app-add-users',
  templateUrl: './add-users.component.html',
  styleUrls: ['./add-users.component.css']
})
export class AddUsersComponent implements OnInit {
  reportString: any = 0;
  addUserForm: FormGroup = new FormGroup({});
  count_user_click: any = 0;
  selected = 'None'
  reportArr: any = [];
  medicineArr: any = [];
  timeArr: any = [];
  doctrArr: any = [];
  medicine: any = [];
  selectedDay: any;
  Time1: string = '';
  mediaSub: Subscription = new Subscription;
  Result: any;
  Frequency: any;
  imageArr: any = [];
  docName:any =[];
  selectedDocName:any;
  counter:any=1;

  constructor(private formBuilder: FormBuilder,
    private userService: UserService,
    private _snackBar: MatSnackBar,
    private MediaObserver: MediaObserver) { }


  ngOnInit(): void {
    this.addUserForm = this.formBuilder.group({
      'fullName': new FormControl('', [Validators.required, Validators.minLength(3)]),
      'phone': new FormControl('', [Validators.required, Validators.maxLength(10)]),
      'disease': new FormControl('', [Validators.required, Validators.minLength(3)]),
      'email': new FormControl('', [Validators.email]),
      'doctorsName': new FormControl('', [Validators.required, Validators.minLength(3)]),
      'doctorSpecialist': new FormControl('', [Validators.required, Validators.minLength(3)]),
      'checkupDate': new FormControl('', [Validators.required]),
      'medicineName': new FormControl('', [Validators.required, Validators.minLength(3)]),
      'doctorName': new FormControl('', [Validators.required, Validators.minLength(3)]),
      'duration': new FormControl('', [Validators.required, Validators.maxLength(20), Validators.minLength(1)]),
      'quantity': new FormControl('', [Validators.required, Validators.maxLength(100), Validators.minLength(1)]),
      'medicineType': new FormControl('', []),
      'frequency': new FormControl('', [Validators.maxLength(1)]),
      'time1': new FormControl('', [Validators.maxLength(7)]),
      // 'reportName': new FormControl('', [Validators.required, Validators.minLength(1)]),
      // 'reportDate': new FormControl('', [Validators.required]),
      // 'reportDetails': new FormControl('', [Validators.maxLength(100)]),
      // 'imageUpload': new FormControl('', []),

    })

  }
  createUser() {
    this.AddMoreMedicine();
    this.AddMoreDoctor();
    console.log(this.reportArr)
    const requestData = {
      masterData: this.addUserForm.value,
      medicineDetails: this.medicineArr,
      doctorDetails: this.doctrArr
    }
    // console.log(requestData)
    this.userService.addUser(requestData).subscribe(data => {
      this._snackBar.open("Medicine and Report Added Succesfully...")
    }, err => {
      console.log(err)
      this._snackBar.open("Unable to add medicine and report...Please provide all the fields or check the Phone Number.")
    })
    this.timeArr = [];
    this.medicineArr = [];
    this.imageArr = [];
    this.docName =[];
    this.addUserForm.controls['fullName'].setValue('');
    this.addUserForm.controls['phone'].setValue('');
    this.addUserForm.controls['disease'].setValue('');
    this.addUserForm.controls['doctorsName'].setValue('');
    this.addUserForm.controls['doctorSpecialist'].setValue('');
    this.addUserForm.controls['checkupDate'].setValue('');
    this.addUserForm.controls['email'].setValue('');
  }
  keyUpEvent(value: any) {
    this.reportString = value.value.length;
  }
  AddMoreDoctor() {
    this.docName.push(this.addUserForm.value.doctorsName)
    let doctorObj = {
      doctorsName: this.addUserForm.value.doctorsName,
      doctorSpecialist: this.addUserForm.value.doctorSpecialist,
      checkupDate: this.addUserForm.value.checkupDate,
    }
    if (doctorObj.doctorsName != "" && doctorObj.doctorSpecialist != "" && doctorObj.checkupDate != "") {
      this.doctrArr.push(doctorObj);
      this.addUserForm.controls['doctorsName'].setValue('');
      this.addUserForm.controls['doctorSpecialist'].setValue('');
      this.addUserForm.controls['checkupDate'].setValue('');
    }
  }
  doctorsList(){
  if(this.counter==1)
    this.docName.push(this.addUserForm.value.doctorsName)
    this.counter++;
  }
  AddMoreMedicine() {
    this.Result = this.selectedDay;
    // console.log(this.Result)
    this.timeArr.push(this.addUserForm.value.time1);
    if (this.Result == 0) {
      this.Frequency = this.timeArr.length;
    }
    else {
      this.Frequency = this.addUserForm.value.medicineType
    }
    // console.log(this.Frequency)
    let medicineObj = {
      medicineName: this.addUserForm.value.medicineName,
      doctorName: this.addUserForm.value.doctorName,
      duration: this.addUserForm.value.duration,
      medicineType: this.addUserForm.value.medicineType,
      quantity: this.addUserForm.value.quantity,
      frequency: this.Frequency,
      time1: this.timeArr
    }
    // console.log(medicineObj)
    if (medicineObj.medicineName != "" && medicineObj.duration != "" && medicineObj.time1 != "" && medicineObj.quantity != "" && medicineObj.doctorName != "") {
      this.medicineArr.push(medicineObj);
      this.addUserForm.controls['medicineName'].setValue('');
      this.addUserForm.controls['doctorName'].setValue('');
      this.addUserForm.controls['duration'].setValue('');
      this.addUserForm.controls['medicineType'].setValue('none');
      this.addUserForm.controls['frequency'].setValue('');
      this.addUserForm.controls['time1'].setValue('');
      this.addUserForm.controls['quantity'].setValue('');
    }
    this.timeArr = [];
  }
  selectChangeHandler(event: any) {
    // console.log(event)
    this.selectedDay = event.value;
    this.addUserForm.controls['frequency'].setValue(event.value);
  }
  AddTime(time: any) {
    // console.log(time)
    this.timeArr.push(time);
    this.addUserForm.controls['time1'].setValue('');
    // console.log(this.timeArr);
  }
}

