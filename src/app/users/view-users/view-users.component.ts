import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatIconModule } from '@angular/material/icon';
import * as XLSX from 'xlsx'
import { max } from 'rxjs';

@Component({
  selector: 'app-view-users',
  templateUrl: './view-users.component.html',
  styleUrls: ['./view-users.component.css']
})
export class ViewUsersComponent implements OnInit {
  MedicineName: any;
  CurrentDate: any;
  checked: boolean = false;
  constructor(private userService: UserService, private activatedRoute: ActivatedRoute,
    private _snackBar: MatSnackBar, private router: Router
  ) { }

  fileName = 'ExcelSheet.xlsx'
  id: any;
  checkBox: any = [];
  userId: any;
  userProfile: any;
  masterData: any;
  currentDate: any;
  medicineTime: any;
  finalObj: any;
  message: any;
  medicineId: any = [];
  Medicine_Intake_time: any;
  finalResult: any = [];
  duration: any;

  ngOnInit(): void {
    // this.userId = this.activatedRoute.snapshot.paramMap.get('id')
    this.activatedRoute.params.subscribe(data => {
      this.userId = data['id'];
      // console.log(this.userId)
    })
    this.userService.viewUsers(this.userId).subscribe(data => {
      this.userProfile = data
      // console.log(this.userProfile)
      this.masterData = this.userProfile.masterData;
      // console.log(this.masterData)
      this.currentDate = this.userProfile.currentDate;
      this.medicineTime = this.masterData.Medicine_Intake_Time_per_Day;
      // console.log(this.masterData)
    })
  }
  submit() {
    this.finalObj = {
      medicineIds: this.medicineId,
      originalData: this.userProfile
    };
    this.userService.medicineDetails(this.finalObj).subscribe(data => {
      // console.log(data)
      this.message = data
      // console.log(this.message)
      this._snackBar.open(this.message.message)

    })
    this.checked = false
    this.router.navigate(['/users/list'])
  }
  addValue(medicineValue: any, event: any) {
    // console.log(event._id)
    console.log(event._checked)
    this.checkBox.push(event._checked)
    this.medicineId.push(medicineValue._id);
    if(event._checked==true){
      this.checked = true
    }else{
    this.checked = false
    }
    // console.log(this.checkBox)

  }
  exportExcel() {
    console.log(this.masterData)
    var list;
    var date: Date;
    this.masterData.forEach((v: any) => {
      date = new Date();
      v.Medicine_Intake_Time_per_Day.forEach((element: any) => {
        this.MedicineName = v.Medicine_Name;
        this.Medicine_Intake_time = element;
        list = {
          "Current_Date": date,
          "Medicine_Name": this.MedicineName,
          "Medicine_Intake_Time": this.Medicine_Intake_time,
          "Select If Taken": ""
        }
        console.log(list)
        this.finalResult.push(list)
      });
    });


    const ws: XLSX.WorkSheet = XLSX.utils.json_to_sheet(this.finalResult);
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
    XLSX.writeFile(wb, this.fileName)
  }


}
