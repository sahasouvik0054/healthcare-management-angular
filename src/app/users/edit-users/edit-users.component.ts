import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute , Router} from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-edit-users',
  templateUrl: './edit-users.component.html',
  styleUrls: ['./edit-users.component.css']
})
export class EditUsersComponent implements OnInit {
  reportString: any = 0;
  count_user_click: any = 0;
  selected = 'None'
  reportArr: any = [];
  medicineArr: any = [];
  medicine: any = [];
  selectedDay: any;
  Time1: string = '';
  timeArr: any = [];
  Result: any;
  medicineDetails: any;
  reportDetails: any;
  doctorDetails: any;
  userId: any;
  UserDetails: any;
  editUserForm: FormGroup = new FormGroup({});
  dataLoaded: boolean = false;
  timeObj: any;
  multipleImages: any = [];
  imageArr: any = [];
  imageUploadResult: any;

  constructor(private activatedRoute: ActivatedRoute,
    private userService: UserService,
    private formBuilder: FormBuilder,
    private _snackBar: MatSnackBar,
    private router: Router) { }

  ngOnInit(): void {
    this.dataLoaded = false;
    this.activatedRoute.params.subscribe(data => {
      this.userId = data['id']
    });

    if (this.userId != '') {
      this.userService.viewUsers(this.userId)
        .toPromise()
        .then(data => {
          this.UserDetails = data;
          Object.assign(this.UserDetails, data);
          // console.log(this.UserDetails)
          this.reportDetails = this.UserDetails.fullDetails.Report_Details;
          console.log(this.reportDetails)

          this.UserDetails.masterData.forEach((element: any) => {
            this.medicineDetails = {
              Medicine_Name: element.Medicine_Name,
              Medicine_Duration: element.Medicine_Duration,
              Doctors_Name: element.Doctors_Name,
              Medicine_Intake_Type: element.Medicine_Intake_Type,
              Medicine_Quantity: element.Medicine_Quantity,
              Medicine_Intake_per_Day: element.Medicine_Intake_per_Day,
              Medicine_Intake_Time_per_Day: element.Medicine_Intake_Time_per_Day,
            }
            element.Medicine_Intake_Time_per_Day.forEach((element1: any) => {
              this.timeObj = { element1 }
            })
          })
          this.UserDetails.fullDetails.Doctor_Details.forEach((element: any) => {
            this.doctorDetails = {
              Doctors_Specialist: element.Doctors_Specialist,
              Next_Checkup_Date: element.Next_Checkup_Date,
              Doctors_Name: element.Doctors_Name,
            }
          })
          this.editUserForm = this.formBuilder.group({
            'fullName': new FormControl(this.UserDetails.fullDetails.patient_details.Patients_name, [Validators.required, Validators.minLength(3)]),
            'phone': new FormControl(this.UserDetails.fullDetails.patient_details.Patient_Phone_Number, [Validators.required, Validators.maxLength(10)]),
            // 'disease': new FormControl(this.UserDetails.fullDetails.patient_details.Infirmity_Occured, [Validators.required, Validators.minLength(3)]),
            // 'email': new FormControl(this.UserDetails.fullDetails.patient_details.Patients_email, [Validators.required, Validators.email]),
            'doctorsName': new FormControl(this.doctorDetails.Doctors_Name, [Validators.required, Validators.minLength(3)]),
            'doctorSpecialist': new FormControl(this.doctorDetails.Doctors_Specialist, [Validators.required, Validators.minLength(3)]),
            'checkupDate': new FormControl(this.doctorDetails.Next_Checkup_Date, [Validators.required]),
            // 'medicineName': new FormControl(this.medicineDetails.Medicine_Name, [Validators.required, Validators.minLength(3)]),
            // 'doctorName': new FormControl(this.medicineDetails.Doctors_Name, [Validators.required, Validators.minLength(3)]),
            // 'duration': new FormControl(this.medicineDetails.Medicine_Duration, [Validators.required, Validators.maxLength(20), Validators.minLength(1)]),
            // 'quantity': new FormControl(this.medicineDetails.Medicine_Quantity, [Validators.required, Validators.maxLength(100), Validators.minLength(1)]),
            // 'medicineType': new FormControl(this.medicineDetails.Medicine_Intake_Type, []),
            // 'frequency': new FormControl(this.medicineDetails.Medicine_Intake_per_Day, [Validators.maxLength(1)]),
            // 'time1': new FormControl(this.medicineDetails.Medicine_Intake_Time_per_Day, [Validators.maxLength(7)]),
            'reportName': new FormControl('', [Validators.required, Validators.minLength(1)]),
            'reportDate': new FormControl('', [Validators.required]),
            'reportDetails': new FormControl('', [Validators.maxLength(100)]),
            'imageUpload': new FormControl('', []),
          })
          this.dataLoaded = true;
        })
        .catch(err => {
          console.log(err);
        })
    }
  }
  updateUser() {
    this.AddMoreReport()
    console.log(this.reportArr)
    const requestData = {
      masterData: this.editUserForm.value,
      // medicineDetails: this.medicineArr,
      reportDetails: this.reportArr,
    }
    this.userService.updateUser(this.userId, requestData).subscribe(data => {
      this._snackBar.open("Patient data updated Succesfully...")
    }, err => {
      this._snackBar.open("Unable to update Patient data...")

    })
    this.router.navigate(['/users/list'])
  }
  uploadFile(event: any) {
    if (event.target.files.length > 0) {
      this.multipleImages = event.target.files;
    }
    // console.log(this.multipleImages)
    const formdata = new FormData()

    for (let img of this.multipleImages) {
      formdata.append('files', img)
    }

    this.userService.imageUpload(formdata).subscribe(data => {
      // console.log(data)
      this.imageUploadResult = data;
      this.imageUploadResult.forEach((element: any) => {
        this.imageArr.push(element);
      });

    }, err => {
      console.log(err)
    })
    console.log(this.imageArr)
  }
  keyUpEvent(value: any) {
    this.reportString = value.value.length;
  }
  AddMoreReport() {
    console.log(this.imageArr)
    let reportObj = {
      reportName: this.editUserForm.value.reportName,
      reportDate: this.editUserForm.value.reportDate,
      reportDetails: this.editUserForm.value.reportDetails,
      imageUpload:this.imageArr
    }

    if (reportObj.reportName != "" && reportObj.reportDate != "" && reportObj.reportDetails != "") {
      this.reportArr.push(reportObj);
      console.log(this.reportArr)
      this.editUserForm.controls['reportName'].setValue('');
      this.editUserForm.controls['reportDate'].setValue('');
      this.editUserForm.controls['reportDetails'].setValue('');
      this.editUserForm.controls['imageUpload'].setValue('');
    }
    // console.log(this.reportArr)
    this.imageArr=[]
  }

}
