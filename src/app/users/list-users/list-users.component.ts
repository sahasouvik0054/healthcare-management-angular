import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { UserService } from 'src/app/services/user.service';
import { FormArray, FormBuilder, FormControl, FormGroup, FormsModule, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MediaChange, MediaObserver } from '@angular/flex-layout';
import { Subscription } from 'rxjs';





// export interface PeriodicElement {
//   Full_Name: string;
//   position: number;
//   Disease: number;
//   MedicineName: string;
//   MedicineDuration:Number;
//   ReportName:String;
//   Doc_Name:String;
// }
// const ELEMENT_DATA: PeriodicElement[] = [
//   {position: 1, Full_Name: 'Hydrogen', Disease: 1.0079, Doc_Name:'hello', MedicineName: 'H' , MedicineDuration:5 , ReportName:'Hello'},
//   {position: 2, Full_Name: 'Helium', Disease: 4.0026, Doc_Name:'hello', MedicineName: 'He' , MedicineDuration:5, ReportName:'Hello'},
//   {position: 3, Full_Name: 'Lithium', Disease: 6.941, Doc_Name:'hello', MedicineName: 'Li' , MedicineDuration:5, ReportName:'Hello'},
//   {position: 4, Full_Name: 'Beryllium', Disease: 9.0122, Doc_Name:'hello', MedicineName: 'Be' , MedicineDuration:5, ReportName:'Hello'},
//   {position: 5, Full_Name: 'Boron', Disease: 10.811, Doc_Name:'hello', MedicineName: 'B' , MedicineDuration:5, ReportName:'Hello'},
//   {position: 6, Full_Name: 'Carbon', Disease: 12.0107, Doc_Name:'hello', MedicineName: 'C' , MedicineDuration:5, ReportName:'Hello'},
//   {position: 7, Full_Name: 'Nitrogen', Disease: 14.0067, Doc_Name:'hello', MedicineName: 'N' , MedicineDuration:5, ReportName:'Hello'},
//   {position: 8, Full_Name: 'Oxygen', Disease: 15.9994, Doc_Name:'hello', MedicineName: 'O' , MedicineDuration:5, ReportName:'Hello'},
//   {position: 9, Full_Name: 'Fluorine', Disease: 18.9984, Doc_Name:'hello', MedicineName: 'F' , MedicineDuration:5, ReportName:'Hello'},
//   {position: 10, Full_Name: 'Neon', Disease: 20.1797, Doc_Name:'hello', MedicineName: 'Ne' , MedicineDuration:5, ReportName:'Hello'},
// ];

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {
  // displayedColumns: string[] = ['position' , 'Full_Name' ,  'Disease' ,'Doc_Name' , 'MedicineName' ,'MedicineDuration' , 'ReportName'];
  // dataSource = ELEMENT_DATA;

  addUserForm: FormGroup = new FormGroup({});
  mediaSub: Subscription = new Subscription;

  masterData: any;
  result: any;
  PatientMobile: any;

  constructor(private formBuilder: FormBuilder,
    private userService: UserService,
    private _snackBar: MatSnackBar,
    private MediaObserver: MediaObserver) { }

  ngOnInit(): void {

    this.addUserForm = this.formBuilder.group({
      'PhoneNo': new FormControl('', [Validators.required, Validators.maxLength(10)]),
    })
  }
  FindUser() {
    const requestData = {
      masterData: this.addUserForm.value,
    }
    // console.log(requestData)
    this.userService.listUsers(requestData).subscribe(data => {

      this.result = data;
      // console.log(this.result)

      this.PatientMobile = this.result[0]?.patient_details.Patient_Phone_Number ;
      // console.log(this.result[0].patient_details.Patient_Phone_Number)
      if (this.result.length > 0) {
        this._snackBar.open("Patient Mobile Number found succesfully")
      }
      else {
        this._snackBar.open("Patient Mobile Number Not Found...Please Enter Correct Mobile Number.")
      }
    }, err => {
      console.log(err)
      this._snackBar.open("An Unknown Error Occurred...Please try after sometime.")
    });
  }

}
