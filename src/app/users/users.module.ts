import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {MatCardModule} from '@angular/material/card';
import { ViewUsersComponent } from './view-users/view-users.component';
import { MatListModule } from '@angular/material/list';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { ReactiveFormsModule } from '@angular/forms';
import {MatSnackBarModule, MAT_SNACK_BAR_DEFAULT_OPTIONS} from '@angular/material/snack-bar';
import { MatButtonModule } from '@angular/material/button';
import { MatNativeDateModule } from '@angular/material/core';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatTableModule} from '@angular/material/table';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import {MatRadioModule} from '@angular/material/radio';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatSelectModule} from '@angular/material/select';
import {MatCheckboxModule} from '@angular/material/checkbox';

@NgModule({
  declarations: [
    ViewUsersComponent,
    
  ],
  imports: [
    CommonModule,
    RouterModule,
    MatCardModule,
    MatListModule,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatButtonModule,
    MatNativeDateModule,
    MatGridListModule,
    MatTableModule,
    NgxMaterialTimepickerModule,
    MatRadioModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatCheckboxModule
  ],
  exports:[
    MatCardModule,
    MatListModule,
    MatInputModule,
    MatFormFieldModule,
    MatIconModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatButtonModule,
    MatNativeDateModule,
    MatGridListModule,
    MatTableModule,
    NgxMaterialTimepickerModule,
    MatRadioModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatCheckboxModule
   
   
  ],
  providers:[
    { provide : MAT_SNACK_BAR_DEFAULT_OPTIONS , useValue:{duration:2500}}
  ]

})
export class UsersModule { }
