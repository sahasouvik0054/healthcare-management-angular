import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-delete-users',
  templateUrl: './delete-users.component.html',
  styleUrls: ['./delete-users.component.css']
})
export class DeleteUsersComponent implements OnInit {

  userId:String='';
  message:any;
  Finaldata: any;
  constructor(private activatedRoute:ActivatedRoute ,
     private userService:UserService , 
     private _snackBar:MatSnackBar,
     private router:Router) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(data =>{
      this.userId = data['id'];
    });
    if(this.userId){
      this.userService.deleteUser(this.userId).subscribe(data =>{
        console.log(data)
        this.message = data;
        this.Finaldata = this.message.data;
        console.log(this.Finaldata)
        this._snackBar.open(this.message.message);
        // this.router.navigate(['list']);
      },err=>{
        this._snackBar.open(err)
      })
    }
  }

}
