import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  baseUrl: String = 'http://localhost:8000';

  listUsers(userobj: any) {
    return this.http.post(this.baseUrl + '/PatientMobile', userobj)
  }
  imageUpload(userobj: any) {
    return this.http.post(this.baseUrl + '/imageUpload', userobj)
  }
  medicineDetails(medicineObj:any) {
    return this.http.post(this.baseUrl + '/fetchMedicine', medicineObj)
  }
  viewUsers(id: any = null) {
    return this.http.get(this.baseUrl + '/readData/' + id)
  }
  addUser(userobj: any) {
    return this.http.post(this.baseUrl + '/create', userobj);
  }
  deleteUser(id: String) {
    return this.http.delete(this.baseUrl + '/delete/' + id);
  }
  updateUser(id: String, userObj: any) {
    return this.http.post(this.baseUrl + '/update/' + id, userObj);
  }
}
